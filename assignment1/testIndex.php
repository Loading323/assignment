<?php

try
{
	$handler = new PDO('mysql:host=localhost;dbname=oblig_1;charset=utf8mb4', 'root', '');
	$handler->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) 
	{
		echo $e->getMessage();
		//die();
	}
	
	$query = $handler->query('SELECT * FROM books');
	while ($r = $query->fetch()) 
	{
		echo $r['title'], '<br>';
	}
